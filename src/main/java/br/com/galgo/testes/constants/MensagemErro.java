package br.com.galgo.testes.constants;

public class MensagemErro {

	public static final String DIRETORIO_NAO_ENCONTRADO = "Não foi encontrado a pasta para salvar evidência de erro!";
	public static final String ERRO_CARREGAR_XML = "Erro ao carregar projeto xml atraves do caminho: ";
	public static final String ERRO_GENERICO = "Erro ao testar servico.";
	public static final String STATUS_INVALIDO = "Servico finalizou com status inválido, diferente de OK.";

}
