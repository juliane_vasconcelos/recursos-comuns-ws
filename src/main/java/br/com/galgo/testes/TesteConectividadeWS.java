package br.com.galgo.testes;

import org.junit.Before;
import org.junit.Test;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import static br.com.galgo.testes.recursos_comuns.utils.ConstantesWS.*;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;
import br.com.galgo.testes.utils.TesteWSUtils;

import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCaseRunner;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStep;
import com.eviware.soapui.support.types.StringToObjectMap;

public class TesteConectividadeWS {

	private final String PASTA_TESTE = "ConectividadeWS";
	private Ambiente ambiente;

	@Before
	public void setUp() throws Exception {
		ambiente = TesteUtils.configurarTeste(Ambiente.PRODUCAO, PASTA_TESTE);
	}

	@Test
	public void testConectividade() throws Exception {
		final WsdlTestCase testCase = TesteWSUtils.getTestCase(TEST_SUITE_NAME,
				TEST_CASE_NAME, ambiente);
		final WsdlTestStep testStep = TesteWSUtils.getTestStep(testCase,
				TEST_STEP_NAME_CONECTIVIDADE);

		final WsdlTestCaseRunner runner = new WsdlTestCaseRunner(testCase,
				new StringToObjectMap());
		final WsdlTestRequestStepResult runTestStep = (WsdlTestRequestStepResult) runner
				.runTestStep(testStep);

		TesteWSUtils.gerarRelatorio(runTestStep, testStep.getName());

		TesteWSUtils.validaTestStep(runTestStep);
	}

}